KillAds
=======

This role will block all ads on network (To use with HostAPDVPN)

Example Playbook
----------------

```yaml
- hosts: target
  become: yes
  become_user: root
  roles:
    - KillAds
```

License
-------

Lucas

Author Information
------------------

Soares Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/NAnsible/KillAds

