#!/bin/bash

i=0

TOTAL_COUNT=`wc -l AdList | cut -d' ' -f1`

for ip in `cat AdList`
do
	i=`expr $i + 1`
	echo -e "Blocking $ip\t($i/$TOTAL_COUNT)"
	iptables -A OUTPUT -d $ip -j REJECT
done
