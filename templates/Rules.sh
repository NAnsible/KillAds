#!/bin/bash

{% for AdProviderIP in AdProviderIPList %}
iptables -A OUTPUT -d {{ AdProviderIP }} -j REJECT
iptables -A INPUT -d {{ AdProviderIP }} -j DROP
{% endfor %}
